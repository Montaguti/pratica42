/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author montaguti
 */
public class Elipse implements FiguraComEixos {
    
    private double xAxis;
    private double yAxis;

    public Elipse(double xAxis, double yAxis) {
        this.xAxis = xAxis;
        this.yAxis = yAxis;
    }

    public double getxAxis() {
        return xAxis;
    }

    public void setxAxis(double xAxis) {
        this.xAxis = xAxis;
    }

    public double getyAxis() {
        return yAxis;
    }

    public void setyAxis(double yAxis) {
        this.yAxis = yAxis;
    }
    
    public double getArea() {
        double r = this.xAxis/2;
        double s = this.yAxis/2;
        return Math.PI * r * s;
    }
    
    public double getPerimetro() {
        double r = this.xAxis/2;
        double s = this.yAxis/2;
        return Math.PI * ((3*(r + s)) - Math.sqrt((3*r+s)*(r+3*s)));
    }

    @Override
    public double getEixoMenor() {
        return (this.xAxis < this.yAxis) ? this.xAxis : this.yAxis;
    }

    @Override
    public double getEixoMaior() {
        return (this.xAxis > this.yAxis) ? this.xAxis : this.yAxis;
    }

    @Override
    public String getNome() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    
    
}