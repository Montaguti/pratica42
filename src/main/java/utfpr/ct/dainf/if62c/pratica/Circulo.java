/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author montaguti
 */
public class Circulo extends Elipse {
    
    private double raio;
    
    public Circulo(double raio) {
        super(raio*2, raio*2);
        this.raio = raio;
    }

    public double getRaio() {
        return raio;
    }

    public void setRaio(double raio) {
        this.raio = raio;
    }
    
    @Override
    public double getPerimetro() {
        return Math.PI * 2 * this.getRaio();
    }

}

